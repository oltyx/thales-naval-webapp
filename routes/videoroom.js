var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.sendFile(__dirname + '/videoroomtest.html');
});

module.exports = router;
