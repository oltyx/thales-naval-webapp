**Running environment**

This was tested on an **Ubuntu** 18.04.5 LTS machine.
Development was done in **WebStorm** 2017.3 with OpenJDK JVM from JetBrains

**Dependencies**

- **Node** v12.19.0 or higher
- **Npm** 6.14.8 or higher

**How to run**

1. Clone the git: `git clone https://gitlab.com/oltyx/thales-naval-webapp.git`
2. Change directory: `cd thales-naval-webapp/`
3. Download dependencies: `npm install`
4. Start the web server: `node bin/www`
5. Open the web application in a browser at address: `localhost:[`**PORT_NUMBER**`]`. The default port number is **3000**.
6. Press _Start_ and select from the _Streams list_ the _H.264 live stream coming from FFmpeg (live) item_

**How to use the Video Room plugin with the Gstreamer WebRTC peer implementation**

7. To access the video room, go to this address: `http://localhost:3000/videoroom`
   To access the the screen sharing page (which uses internaly the Video Room plugin) go to this address: `http://localhost:3000/screensharing`

! Be sure Janus is already running ! 