var janus = new Janus.Client('ws://172.17.0.2:8188', {
    keepalive: 'true'
});

$(document).ready(function() {
    $('#start').one('click', function() {

        janus.createConnection('client').then(function(connection) {
            connection.createSession().then(function(session) {
                console.log("Connection created!");
                session.attachPlugin('janus.plugin.streaming').then(function(plugin) {
                    console.log("Session created!");
                    plugin.send({
                                    "janus" : "message",
                                    "body"  : {
                                        "request" : "list"
                                    }
                                 }).then(function(response){
                                     console.log(response);
                    });
                    // plugin.on('Streaming plugin is on!', function(message) {});
                    plugin.detach();
                });
            });
        });

    });
});